# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Ionut Biru <ibiru@archlinux.org>
# Sébastien Luttringer <seblu@aur.archlinux.org>

_linuxprefix=linux-xanmod-lts
_extramodules=extramodules-6.1-xanmod
pkgbase=${_linuxprefix}-virtualbox-modules
pkgname=("${_linuxprefix}-virtualbox-host-modules")
_pkgver=7.0.10
pkgver=7.0.10_6.1.39.xanmod1_1
pkgrel=1
arch=('x86_64')
url='https://virtualbox.org'
license=('GPL')
groups=("${_linuxprefix}-extramodules")
makedepends=("virtualbox-host-dkms=${_pkgver}"
             'dkms'
             "${_linuxprefix}" "${_linuxprefix}-headers")

pkgver() {
    _ver=$(pacman -Q ${_linuxprefix} | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
  # build host modules
  msg2 'Host modules'
  fakeroot dkms build --dkmstree "${srcdir}" -m "vboxhost/${_pkgver}_OSE" -k "${_kernver}"
}

package_linux-xanmod-lts-virtualbox-host-modules(){
  pkgdesc='Host kernel modules for VirtualBox'
  _ver=$(pacman -Q ${_linuxprefix} | cut -d " " -f 2)
  depends=("${_linuxprefix}=${_ver}")
  replaces=("${_linuxprefix}-virtualbox-modules" 'linux510-xanmod-virtualbox-host-modules')
  conflicts=("${_linuxprefix}-virtualbox-modules")
  provides=('VIRTUALBOX-HOST-MODULES')
  install=virtualbox-host-modules.install

  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  install -dm755 "${pkgdir}/usr/lib/modules/${_extramodules}"
  cd "vboxhost/${_pkgver}_OSE/${_kernver}/$CARCH/module"
  install -m644 * "${pkgdir}/usr/lib/modules/${_extramodules}"
  find "${pkgdir}" -name '*.ko' -exec gzip -9 {} +
  sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/virtualbox-host-modules.install"

  mkdir -p "${pkgdir}/etc/modules-load.d"
  echo "vboxdrv" > "${pkgdir}/etc/modules-load.d/${_linuxprefix}-virtualbox-host-modules.conf"
  echo "vboxnetadp" >> "${pkgdir}/etc/modules-load.d/${_linuxprefix}-virtualbox-host-modules.conf"
  echo "vboxnetflt" >> "${pkgdir}/etc/modules-load.d/${_linuxprefix}-virtualbox-host-modules.conf"
}
